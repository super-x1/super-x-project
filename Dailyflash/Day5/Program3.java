//WAP to check whether the given number is a strong number or not.

import java.util.*;
public class Program3 {

        static int factorial(int num){

            int fact=1;
            for(int i=1;i<=num;i++){
                fact=fact*i;
            }
            return fact;
        }

         static void isStrong(int num){
            
            int temp=num;
            int sum=0;

            while(temp!=0){

                sum=sum+factorial(temp%10);
                temp=temp/10;
            }
            if(sum==num){
                System.out.println("strong number:");
            }
            else{
                System.out.println("not strong number");
            }
        }

        public static void main(String[]args){

            Scanner sc=new Scanner(System.in);
            System.out.println("Enter number:");

            int num=sc.nextInt();

            isStrong(num);
        }
    }
