//WAP to print the factorial of digits in a given range.


import java.util.*;
public class Program1 {
    public static void main(String [] args){

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter First number:");
        int start=sc.nextInt();

        System.out.println("Enter Second number:");
        int end=sc.nextInt();

        int fact=1;

        for(int i=start;i<=end;i++){

            fact=fact*i;
        }
        System.out.println("fact = "+fact );
    }   

    }