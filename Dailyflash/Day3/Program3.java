import java.util.*;

 class Program3 {

        int isPalindrome(int x) {
        
        if (x < 0 ) {
            return -1;
        }

        int rev= 0;

         while (x != 0) {
            int digit = x % 10;
            rev = rev * 10 + digit;
            x = x/10;
        }

        return rev;
    }

    public static void main(String[] args) {

            Scanner sc = new Scanner(System.in);
            System.out.println("Enter Number: ");
            int num = sc.nextInt(); 
            sc.close();

            Program3 obj=new Program3();
            int result = obj.isPalindrome(num);

            if(result==num){
                    System.out.println(num+" is palindrome no");
            }else{
                    System.out.println(num+" is not palindrome no");
            }
        
        }
    }

