import java.util.*;
class Program2 {

    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number of rows:");
        int row=sc.nextInt();
        sc.close();

        for(int i=1;i<=row;i++){
            int temp=i;
            for(int j=1;j<=i;j++){

                System.out.print(temp+" ");
                temp--;
            }
            System.out.println(" ");
        }
    }
    
}
