// WAP to print sum of digits in given range

import java.util.*;
public class Program4 {
    public static void main(String [] args){

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter First number:");
        int start=sc.nextInt();

        System.out.println("Enter Second number:");
        int end=sc.nextInt();

        int sum=0;

        for(int i=start;i<=end;i++){

            sum=sum+i;
        }
        System.out.println("Sum = "+sum );
        

    }
    
}
