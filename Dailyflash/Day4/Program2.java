
import java.util.*;
class Program2 {

    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number of rows:");
        int row=sc.nextInt();
        sc.close();
    
        int ch=64;
        //char ch1 =(char)ch;
        for(int i=1;i<=row;i++){
            
            for(int j=1;j<=i;j++){
                
                System.out.print(ch+" ");
                ch--;
            }
            System.out.println(" ");
        }
    }
    
}
