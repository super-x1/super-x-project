
import java.util.*;
class Program3{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number: ");
		int num=sc.nextInt();
		sc.close();

		if(num%2!=0){

			System.out.println(num+" is odd number");
		}
		else{
			System.out.println(num+" is even number");
		}
	}
}

