

import java.util.Scanner;

public class Program4 {

    static int factorial(int num){

            int fact=1;
            for(int i=1;i<=num;i++){
                fact=fact*i;
            }
            return fact;
        }

         static void isStrong(int start,int end){
            
            
            int sum=0;

            for(int i=start;i<=end;i++){
                
                int temp=i;
                while(temp!=0){

                    sum=sum+factorial(temp%10);
                    temp=temp/10;
            }
            if(sum==i){
                System.out.println(i+" ");
            }
            
        }
    }
        public static void main(String[]args){

            Scanner sc=new Scanner(System.in);
            System.out.println("Enter starting number:");
            int start=sc.nextInt();

            System.out.println("Enter ending number:");
            int end=sc.nextInt();

            isStrong(start,end);
        }
    }

    

