

/* WAP to check whether the given number is perfect or not */


import java.util.*;
public class Program3 {

    public static int isPerfect(int num){

        int sum=0;
        for(int i=1;i<=num/2;i++){

            if(num%i==0){

                sum=sum+i;
            }
        }
        return sum;
    }
    public static void main(String[]args){

        Scanner sc=new Scanner(System.in);
        System.out.println("Enter number:");
        int num=sc.nextInt();

        int result=isPerfect(num);

        if(result==num){

            System.out.println(num+" is perfect number");
        }
        else{

            System.out.println(num+" is not perfect number");
        }
    }
}

