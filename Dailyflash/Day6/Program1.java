

import java.util.*;
public class Program1 {

    public static void main(String[]args){

        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Rows:");
        int row=sc.nextInt();

        int num=1;

        for(int i=1;i<=row;i++){

            int ch=65;
            char ch1=(char)ch;

            for(int j=1;j<=row-1;j++){

                if(i%2!=0){

                    System.out.print(ch1+" ");
                    ch1++;
                }else{

                    System.out.print(num +" ");
                    num=num+2;
                }
            }
            System.out.println("");
        }
    }
    
}
