
// WAP to replace vowels to # in given string

import java.util.Scanner;

public class Program5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a string: ");
        String str = scanner.nextLine();

        StringBuilder output = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {

            char ch=str.charAt(i);
            if (ch=='a' || ch=='e' || ch=='i' || ch=='o' || ch=='u') {
                
                  output.append('#');
            }
            else{

                output.append(ch);
            }
        }

    
        System.out.println(output);
    }
}

    
    

