/* WAP to check whether the given number is prime or composite */

import java.util.*;

class Program3{

	 int isPrime(int num){

               
		if(num<2){

			return 0;
		}
		int flag=0;
         	for(int i=2;i<=num/2;i++){

				if(num%i==0){

					flag=1;
				}
		}
		return flag;
		
	 }
}
class main{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number: ");

		int num=sc.nextInt();

		Program3 obj=new Program3();

	        int result=obj.isPrime(num);

		if(result==1){

			System.out.println(num+" is composite number");
		}
		else{
			System.out.println(num+" is prime number");
		}
	}
}

