/* WAP to print Composite numbers betwwen given range */

import java.util.*;
class Program4 {

        void isComposite(int start,int end){

        System.out.println("Composite numbers between " + start + " and " + end );
        for (int i = start; i <= end; i++) {
            if (i <= 1) {
                continue; 
            }
            boolean isComposite = false;
            for (int j = 2; j <= i / 2; j++) {
                if (i % j == 0) {
                    isComposite = true;
                    break;
                }
            }
            if (isComposite) {
                System.out.print(i + " ");
            }
        }
    }
}
class Composite{

    public static void main(String[] args) {

	    Scanner sc=new Scanner(System.in);

	    System.out.println("Enter starting value: ");
	    int start=sc.nextInt();
	    System.out.println("Enter ending value: ");
	    int end=sc.nextInt();

	    Program4 obj= new Program4(); 

	    obj.isComposite(start,end);
    }
}


