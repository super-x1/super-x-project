/* WAP to print the following Pattern take input from user
    A B C D
    B C D E 
    C D E F
    D E F G
*/

import java.util.*;
class Program1{

	public static void main(String[]args){

		Scanner sc=new Scanner (System.in);

		System.out.println("Enter no of rows: ");
		int rows=sc.nextInt();

		
		for(int i=0;i<rows;i++){
			
			int ch = 65+i;
		        char ch1 = (char)ch;

			for(int j=0;j<rows;j++){

				System.out.print(ch1+" ");
				ch1++;
			}
			System.out.println();
		}
	}
}
