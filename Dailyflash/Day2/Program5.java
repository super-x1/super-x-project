/* WAP to check whether the string contains vowels and return count of vowels */

import java.io.*;
class Program5{

	        int VowelCount(String str){

        	int count=0;

		for(int i=0;i<str.length();i++){

			char ch=str.charAt(i);

			if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u'){

                        
				count++;
			
			}
		}
		return count;

	}
}
class Vowel{

	public static void main(String[]args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter string: ");
		String str=br.readLine();

		str=str.toLowerCase();

		Program5 obj=new Program5();

		int result=obj.VowelCount(str);

		if(result>0){

			System.out.println("String "+str+" contains "+result+" Vowels ");
		}
		else{
			System.out.println("String does not contain Vowels");
		}
	}
}
