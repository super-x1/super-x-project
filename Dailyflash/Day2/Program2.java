/* WAP to print the following Pattern take row from user
    1
    2  4
    3  6  9
    4  8  12  16
*/

import java.util.*;
class Program2{

	public static void main(String[]args){

		Scanner sc=new Scanner (System.in);

		System.out.println("Enter no of rows: ");
		int rows=sc.nextInt();

		
		for(int i=1;i<=rows;i++){
			
		        int x=i;

			for(int j=1;j<=i;j++){

				System.out.print(x+" ");
				x=x+i;
			}
			System.out.println();
		}
	}
}
