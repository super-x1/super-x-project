package leetcode.Day2;

import java.util.*;

 class Program1 {

        boolean isPalindrome(int x) {
        
        if (x < 0 ) {
            return false;
        }

        int rev= 0;
        int num = x;

         while (x != 0) {
            int digit = x % 10;
            rev = rev * 10 + digit;
            x = x/10;
        }

        return num == rev;
    }

    public static void main(String[] args) {

            Scanner sc = new Scanner(System.in);
            System.out.println("Enter Number: ");
            int num = sc.nextInt(); 
            sc.close();

            Program1 obj=new Program1();
            System.out.println(obj.isPalindrome(num));
        }
    }

