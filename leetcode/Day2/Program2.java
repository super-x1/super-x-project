package leetcode.Day2;

class Program2 {
         int StrSearch(String haystack, String needle) {
        
        for (int i = 0; i <= haystack.length()- needle.length(); i++) {
            int found = 0;
            for (int j = 0; j < needle.length(); j++) {
                if (needle.charAt(j)!=haystack.charAt(i + j)) {
                    found = 1;
                    break;
                }
            }
            if (found==0) {
                return i;
            }
        }
        return -1;
    }
    
    public static void main(String[] args) {

    
        Program2 obj = new Program2();
        String haystack = "leetcode";
        String needle = "leeto";
        int output = obj.StrSearch(haystack, needle);
        System.out.println(output);
    }
}
