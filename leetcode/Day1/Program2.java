
import java.util.*;
class Search{

        int searchElement(int[] arr, int target,int size) {
        int start = 0;
        int end = size - 1;
        
        while (start <= end) {
            int mid = (start + end) / 2;
            
            if (arr[mid] == target) {

                    return mid;

            } else if (arr[mid] < target) {
		    
		    start = mid + 1; 
            } else {

                    end = mid - 1; 
            }
        }
        
        return start;
    }
}
class Program2{
    
        public static void main(String[] args){

		Scanner sc= new Scanner(System.in);
		System.out.println("Enter array size");
		int size=sc.nextInt();
                sc.close();
 
		int []arr=new int [size];
		System.out.println("Enter array elements");
		for(int i=0;i<size;i++){

			arr[i]=sc.nextInt();
		}
		System.out.println(" array elements: ");
                          for(int i=0;i<size;i++){

			System.out.println(arr[i]);
		}
		System.out.println(" enter target element: ");
		int target=sc.nextInt();
	 
                Search obj=new Search();
                int result = obj.searchElement(arr, target,size);
        
                System.out.println("Output : " + result);
    }
}

